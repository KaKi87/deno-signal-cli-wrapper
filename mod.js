import { readLines } from 'https://deno.land/std@0.115.1/io/mod.ts';

import signalCliWrapper from 'https://git.kaki87.net/KaKi87/signal-cli-wrapper/raw/branch/master/index.js';

export default ({ signalCliPath }) => signalCliWrapper({
    execute: async args => {
        const command = Deno.run({
            cmd: [signalCliPath, ...args],
            stdin: 'piped',
            stdout: 'piped',
            stderr: 'piped'
        });
        return (
            new TextDecoder().decode(await command.output())
            +
            new TextDecoder().decode(await command.stderrOutput())
        ).trim();
    },
    spawn: ({
        args,
        onData
    }) => {
        const command = Deno.run({
            cmd: [signalCliPath, ...args],
            stdin: 'piped',
            stdout: 'piped',
            stderr: 'piped'
        });
        (async () => {
            await Promise.all([
                (async () => {
                    for await (const data of readLines(command.stdout))
                        onData(data);
                })(),
                (async () => {
                    for await (const data of readLines(command.stderr))
                        onData(data);
                })()
            ]);
        })();
    }
});